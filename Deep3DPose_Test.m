function acc = Deep3DPose_Test(dataset, test_net, net_opt)    
    net_opt.test_num_person = 2;
    net_opt.test_frames_per_person = 1;
    net_opt.test_num_viewpoints = 2;
    
    batch_size = net_opt.test_num_person * net_opt.test_frames_per_person * net_opt.test_num_viewpoints;
    
    test_net.blobs('data').reshape([net_opt.image_size, net_opt.image_size, 3, batch_size]);
    
    acc = 0;
    fprintf('Test start...\n');
    for i = 1:100
        [data_batch, label_batch] = generate_test_batch(dataset, net_opt);
        data_batch = CaffeInputPreProcess(data_batch, label_batch, 'VGG', net_opt);
        test_net.forward({data_batch});
        feature_pm = test_net.blobs('feature_norm').get_data();
        acc = acc + PointsEvaluate(feature_pm, label_batch, 0, net_opt);
        fprintf('\t%d/%d\n', i, 100);
    end
    acc = acc / 100;
    fprintf('Accuracy = %.5f\n', acc);
    test_net.blobs('data').reshape([net_opt.image_size, net_opt.image_size, 3, net_opt.num_person * net_opt.frames_per_person * net_opt.num_viewpoints]);
end