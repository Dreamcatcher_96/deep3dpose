function [diff_feat_1, diff_feat_2] = Contrastive_loss_backward(feat_1, feat_2, label, margin)
% feat_1 : D * N
    [D, N]      = size(feat_1);
    assert(N ~= 0);
    diff        = feat_1 - feat_2;
    dist        = cellfun(@(x) norm(x), mat2cell(diff, D, ones(1, N)));

    diff_feat_1     = zeros(size(feat_1));
    diff_feat_2     = zeros(size(feat_2));
    
    pos             = find(label == 1);
    diff_feat_1(:, pos)     = diff(:, pos);
    diff_feat_2(:, pos)     = -diff(:, pos);
        
    mdist           = margin - dist;
    pick            = setdiff(find(mdist > 0), pos);
    if ~isempty(pick)
        diff_feat_1(:, pick) = -bsxfun(@rdivide, bsxfun(@times, mdist(pick), diff(:, pick)), dist(pick) + 1e-4);
        diff_feat_2(:, pick) =  bsxfun(@rdivide, bsxfun(@times, mdist(pick), diff(:, pick)), dist(pick) + 1e-4);
    end
    
    diff_feat_1     = diff_feat_1 ./ N;
    diff_feat_2     = diff_feat_2 ./ N;
    
%     if label
%         diff_feat_1     = diff; 
%         diff_feat_2     = -diff;       
%     else
%         mdist   = margin - d;
%         if mdist > 0
%             diff_feat_1 = -mdist * diff / (d + 1e-4);
%             diff_feat_2 =  mdist * diff / (d + 1e-4);
%         end
%     end    
%     
    
    % if N ~= 1  diff_feat_1 = diff / N;
    % if N ~= 1  diff_feat_2 =-diff / N;
end

