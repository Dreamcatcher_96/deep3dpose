function caffe_gpu_init(use_gpu, gpu_id)
    caffe.reset_all;
    if use_gpu
        caffe.set_mode_gpu();
        caffe.set_device(gpu_id-1);
    else
        caffe.set_mode_cpu();
    end
end