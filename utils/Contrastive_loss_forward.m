function loss = Contrastive_loss_forward(feat_1, feat_2, label, margin)
% feat_1 : D * N
    [D, N]      = size(feat_1);
    assert(N ~= 0);
    diff        = feat_1 - feat_2;
    dist        = cellfun(@(x) norm(x), mat2cell(diff, D, ones(1, N)));
   
    loss   = sum((label .* dist.^2 + (1 - label) .* max(margin - dist, 0).^2)) / (2 * N); 
end