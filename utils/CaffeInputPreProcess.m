% File Type:     Matlab
% Author:        Hanxi Li {lihanxi2001@gmail.com}
% Creation:      星期六 11/06/2016 13:52.
% Last Revision: 星期六 11/06/2016 13:52.
%qt

function [im_batch, label_batch] = CaffeInputPreProcess(im_batch, label_batch, net_name, net_opt)

    im_batch = single(im_batch);
    
    switch net_name
    case 'VGG'
        im_batch = im_batch(:, :, [3, 2, 1], :); % convert from RGB to BGR
    otherwise
    end

    im_batch = permute(im_batch, [2, 1, 3, 4]); % permute width and height
    if net_opt.multi_task
        label_batch.heatmap = permute(label_batch.heatmap, [2, 1, 3, 4]);
        label_batch.vectormap = permute(label_batch.vectormap, [2, 1, 3, 4]);
    end
end
