function points_index = select_points(vec_ind, label, net_opt)
%     in_ratio = 0.0003;
%     out_ratio = 0.001;
    points_index = [];
    for i = 1:size(vec_ind, 1)
        heatmap = label{vec_ind(i, 1)}.heatmap(:, :, vec_ind(i, 4));
        heatmap = (heatmap - min(heatmap(:))) ./ (max(heatmap(:)) - min(heatmap(:)));
        heatmap = heatmap - rand(size(heatmap));
        k3d = label{vec_ind(i, 1)}.keypoints(vec_ind(i, 4)).coord_3d;
        points_index = [points_index; vec_ind(i, :), 1];
        [y, x] = ind2sub([size(heatmap, 1), size(heatmap, 2)], find(and(heatmap <= 1, heatmap >= -0.01)));   
        if numel(find(x > 46)) > 0 || numel(find(y > 46)) > 0
            disp('debug');
        end
        for ii = 1:numel(x)
            dist = norm([x(ii), y(ii)] - vec_ind(i, 2:3));
%             if dist < 1 / k3d(3) * net_opt.in_ratio / label{vec_ind(i, 1)}.zoom_scale
            if dist < 1.5
                points_index = [points_index; vec_ind(i, :), 1];
                points_index(end, 2:3) = [x(ii), y(ii)];
%             elseif dist > 1 / k3d(3) * net_opt.out_ratio / label{vec_ind(i, 1)}.zoom_scale
            elseif dist > 3
                points_index = [points_index; vec_ind(i, :), 0];
                points_index(end, 2:3) = [x(ii), y(ii)];
            end
        end
    end
end