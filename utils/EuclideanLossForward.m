function [ loc_loss_struct ] = EuclideanLossForward( loc_pred_data, loc_gt_data)

    loc_loss_struct.diff    = zeros(size(loc_pred_data));
    loc_loss_struct.errors  = zeros(size(loc_pred_data));
    loss = (loc_gt_data - loc_pred_data) .^ 2; % + (loc_gt_data(:, 1) - loc_pred_data(:, 1)) .^ 2;
    loc_loss_struct.diff = 2 * (loc_pred_data - loc_gt_data);
    
    loc_loss_struct.loss    = sum(loss(:));
end