function data_processed = data_preprocess(dataset_path)
    if exist('./data/data_h36m.mat', 'file')
        data_processed = load('./data/data_h36m.mat');
        data_processed = data_processed.dataset;
        return;
    end
    dataset = load(dataset_path);
    dataset = dataset.dataset;
    data_processed = {};
    for i = 1:length(dataset)
        data_per_person = {};
        for i_frame = 1:max([length(dataset{i}{1}), length(dataset{i}{2}), length(dataset{i}{3}), length(dataset{i}{4})])
            for i_cam = 1:4
                if isempty(dataset{i}{i_cam}{i_frame})
                    data = [];
                else
                    data.img_path = dataset{i}{i_cam}{i_frame}.img_path;
                    data.type = dataset{i}{i_cam}{i_frame}.type;
                    data.cam_no = dataset{i}{i_cam}{i_frame}.cam_no;
                    data.frame_no = dataset{i}{i_cam}{i_frame}.frame_no;
                    data.key_points = dataset{i}{i_cam}{i_frame}.key_points;
                    data.bbox = dataset{i}{i_cam}{i_frame}.bbox;
                end
                if i_cam == 1
                    data_per_person{i_cam, end+1} = data;
                else
                    data_per_person{i_cam, end} = data;
                end
            end
        end
        data_processed{end+1} = data_per_person;
    end
    MakeDirIfMissing('./data');
    save('./data/data_h36m.mat', 'data_processed');
end