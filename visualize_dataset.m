function visualize_dataset()
    clc;
    clear;
    close all;
    dbstop if error;
    
    dataset = load('./data/data_h36m.mat');
    dataset = dataset.data_processed;
    
    for i = 1:length(dataset)
        for i_frame = 1:size(dataset{i}, 2)
            for i_cam = 1:4
                img_path = dataset{i}{i_cam, i_frame}.img_path;
                keypoints = dataset{i}{i_cam, i_frame}.key_points;
                img = imread(img_path);
                [height, width, ~] = size(img);
                img = imresize(img, [368, 368]);
                imshow(img);
                hold on;
                for i_points = 1:length(keypoints)
                    if ismember(i_points, [2, 3, 4, 6, 7, 8, 12, 13, 15, 16, 17, 21, 22, 23]) 
                        fprintf('%s  %s\n',  keypoints(i_points).id, keypoints(i_points).name);
                        coord = keypoints(i_points).coord;
                        coord(1) = coord(1) / width * 368;
                        coord(2) = coord(2) / height * 368;
                        plot(coord(1), coord(2), '*');
                    end
                end
                pause
            end
        end
    end
end