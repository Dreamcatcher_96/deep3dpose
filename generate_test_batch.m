function [data_batch, label_batch] = generate_test_batch(dataset, net_opt)
    if net_opt.dataset_name == 'H36M'
        points_index = [13, 12, 21, 22, 23, 15, 16, 17, 2, 3, 4, 6, 7, 8];
        missing_index = [15, 16, 17, 18];
        cam_pairs = [1, 3; 2, 4];
    end
    rand_person = randperm(length(dataset), net_opt.test_num_person);
    data_batch = zeros(net_opt.image_size, net_opt.image_size, 3, net_opt.test_num_person * net_opt.test_frames_per_person * net_opt.test_num_viewpoints);
    label_batch = cell(1, net_opt.test_num_person);      
    
    cnt = 1;
    for i = 1:numel(rand_person)
        rand_frame = randperm(length(dataset{rand_person(i)}), net_opt.test_frames_per_person);
        selected_frame = dataset{rand_person(i)}(:, rand_frame);
        select_index = zeros(size(selected_frame), 'logical');
        for i_sel = 1:size(select_index, 2)            
            select_index(cam_pairs(randperm(size(cam_pairs, 1), 1), :), i_sel) = 1;
            while numel(find(~cellfun(@isempty, selected_frame(select_index(:, i_sel))))) < net_opt.test_num_viewpoints
                select_index(:, i_sel) = 0;
                select_index(cam_pairs(randperm(size(cam_pairs, 1), 1), :), i_sel) = 1;
            end
        end
        selected_frame = reshape(selected_frame(select_index), net_opt.test_num_viewpoints, []);
        for i_frame = 1:net_opt.test_frames_per_person
            for j_frame = 1:net_opt.test_num_viewpoints
                img = imread(selected_frame{j_frame, i_frame}.img_path);
%                 [i_height, i_width, i_channel] = size(img);
                
                if net_opt.dataset_name == 'H36M'
                    keypoints = selected_frame{j_frame, i_frame}.key_points(points_index);
                    for missing = missing_index
                        keypoints(missing).name = [];
                    end
                end
%                 joint_coord = extractfield(keypoints, 'coord')';
%                 joint_coord = joint_coord(~cellfun('isempty',joint_coord));
%                 margin = 1 / k3d(3) * 30000;
%                 crop = [min(keypoints, [], 1) - margin, max(keypoints, [], 1) + margin];
%                 img = img(crop(2):crop(4), crop(1):crop(3), :);
                bbox_pred_now = selected_frame{j_frame, i_frame}.bbox;
                kpts = extractfield(keypoints, 'coord');
                kpts(cellfun(@isempty, kpts)) = [];
                kpts = cell2mat(kpts');
                kpt_bound = [min(kpts, [], 1), max(kpts, [], 1)];
                bbox_pred_now(1) = min(kpt_bound(1), bbox_pred_now(1));
                bbox_pred_now(2) = min(kpt_bound(2), bbox_pred_now(2));
                bbox_pred_now(3) = max(kpt_bound(3), bbox_pred_now(3));
                bbox_pred_now(4) = max(kpt_bound(4), bbox_pred_now(4));
                
                bbox_pred_now(1) = bbox_pred_now(1) - (bbox_pred_now(3) - bbox_pred_now(1)) * 0.025;
                bbox_pred_now(2) = bbox_pred_now(2) - (bbox_pred_now(4) - bbox_pred_now(2)) * 0.025;
                bbox_pred_now(3) = bbox_pred_now(3) + (bbox_pred_now(3) - bbox_pred_now(1)) * 0.025;
                bbox_pred_now(4) = bbox_pred_now(4) + (bbox_pred_now(4) - bbox_pred_now(2)) * 0.025;
                
                bbox_pred_now = floor(bbox_pred_now);                
                offset = (bbox_pred_now(3)-bbox_pred_now(1)) - (bbox_pred_now(4)-bbox_pred_now(2));
                if offset > 0
                    bbox_pred_now(2) = bbox_pred_now(2) - offset/2;
                    bbox_pred_now(4) = bbox_pred_now(4) + offset/2;
                elseif offset < 0
                    offset = -offset;
                    bbox_pred_now(1) = bbox_pred_now(1) - offset/2;
                    bbox_pred_now(3) = bbox_pred_now(3) + offset/2;
                end
                bbox_pred_now = int32(bbox_pred_now);
%                 if bbox_pred_now(1) < 0
%                     bbox_pred_now(3) = bbox_pred_now(3) - bbox_pred_now(1);
%                     bbox_pred_now(1) = bbox_pred_now(1) - bbox_pred_now(1); 
%                 end
%                 if bbox_pred_now(2) < 0
%                     bbox_pred_now(4) = bbox_pred_now(4) - bbox_pred_now(2);
%                     bbox_pred_now(2) = bbox_pred_now(2) - bbox_pred_now(2); 
%                 end
%                 if bbox_pred_now(3) < size(img, 2)
%                     bbox_pred_now(3) = bbox_pred_now(3) - bbox_pred_now(1);
%                     bbox_pred_now(1) = bbox_pred_now(1) - bbox_pred_now(1); 
%                 end
%                 if bbox_pred_now(4) < size(img, 1)
%                     bbox_pred_now(3) = bbox_pred_now(3) - bbox_pred_now(1);
%                     bbox_pred_now(1) = bbox_pred_now(1) - bbox_pred_now(1); 
%                 end
                bbox_pred_now(1) = max(1, min(bbox_pred_now(1), size(img, 2)));
                bbox_pred_now(2) = max(1, min(bbox_pred_now(2), size(img, 1)));
                bbox_pred_now(3) = max(1, min(bbox_pred_now(3), size(img, 2)));
                bbox_pred_now(4) = max(1, min(bbox_pred_now(4), size(img, 1)));
                if bbox_pred_now(3) - bbox_pred_now(1) ~= bbox_pred_now(4) - bbox_pred_now(2)
                    if bbox_pred_now(1) == 1
                        bbox_pred_now(3) = bbox_pred_now(3) + abs((bbox_pred_now(3) - bbox_pred_now(1)) - (bbox_pred_now(4) - bbox_pred_now(2)));
                    end
                    if bbox_pred_now(2) == 1
                        bbox_pred_now(4) = bbox_pred_now(4) + abs((bbox_pred_now(3) - bbox_pred_now(1)) - (bbox_pred_now(4) - bbox_pred_now(2)));
                    end
                    if bbox_pred_now(3) == size(img, 2)
                        bbox_pred_now(1) = bbox_pred_now(1) - abs((bbox_pred_now(3) - bbox_pred_now(1)) - (bbox_pred_now(4) - bbox_pred_now(2)));
                    end
                    if bbox_pred_now(4) == size(img, 1)
                        bbox_pred_now(2) = bbox_pred_now(2) - abs((bbox_pred_now(3) - bbox_pred_now(1)) - (bbox_pred_now(4) - bbox_pred_now(2)));
                    end
                end
                [i_height, i_width, i_channel] = size(img);
                img = img(bbox_pred_now(2):bbox_pred_now(4), bbox_pred_now(1):bbox_pred_now(3), :);
                [crop_height, crop_width, i_channel] = size(img);
                zoom_scale = (crop_height / i_height + crop_width / i_width) / 2;
                img = imresize(img, [net_opt.image_size, net_opt.image_size]);
                img = (single(img) - 128) ./ 256;
                data_batch(:, :, :, cnt) = img;
                for i_kpt = 1:numel(keypoints)
                    if isempty(keypoints(i_kpt).id)
                        continue;
                    end
                    if keypoints(i_kpt).coord(1) > i_width || keypoints(i_kpt).coord(1) <= 0 || ...
                            keypoints(i_kpt).coord(2) > i_height || keypoints(i_kpt).coord(2) <= 0
                        keypoints(i_kpt).name = [];
                        keypoints(i_kpt).id = [];
                        keypoints(i_kpt).coord = [];
                        keypoints(i_kpt).coord_3d = [];
                        continue;
                    end
                    if floor((((keypoints(i_kpt).coord(1) - single(bbox_pred_now(1))) / crop_width * net_opt.image_size)-1) / 8) + 1 > 46 ||...
                            floor((((keypoints(i_kpt).coord(2) - single(bbox_pred_now(2))) / crop_height * net_opt.image_size)-1) / 8) + 1 > 46
                        disp('debug');
                    end
                    keypoints(i_kpt).id = i_kpt;
                    keypoints(i_kpt).coord(1) = (keypoints(i_kpt).coord(1) - single(bbox_pred_now(1))) / crop_width * net_opt.image_size;
                    keypoints(i_kpt).coord(2) = (keypoints(i_kpt).coord(2) - single(bbox_pred_now(2))) / crop_height * net_opt.image_size;
                end
                label_batch{cnt}.keypoints = keypoints;
                label_batch{cnt}.heatmap = generate_heatmap(keypoints, net_opt);
                if net_opt.multi_task
                    label_batch{cnt}.vectormap = generate_vectormap(keypoints, net_opt);
                end
                label_batch{cnt}.img_path = selected_frame{j_frame, i_frame}.img_path;
                label_batch{cnt}.cls = rand_person(i);
                label_batch{cnt}.type = selected_frame{j_frame, i_frame}.type;
                label_batch{cnt}.frame_no = selected_frame{j_frame, i_frame}.frame_no;
                label_batch{cnt}.img_size = [i_height, i_width];
                label_batch{cnt}.bbox = selected_frame{j_frame, i_frame}.bbox;
                label_batch{cnt}.zoom_scale = zoom_scale;
                label_batch{cnt}.crop_coord = bbox_pred_now;
                cnt = cnt + 1;
            end
        end
    end

end