function [loss, pairs] = D3PLossForwardBackward(caffe_solver, heatmap, vectormap, pointmap, label, net_opt)
    if net_opt.multi_task
        heatmap_gt = zeros(size(heatmap));
        for i = 1:size(heatmap_gt, 4)
            heatmap_gt(:, :, :, i) = label{i}.heatmap;
        end
        CPM_loss = EuclideanLossForward(heatmap, heatmap_gt);


        vectormap_gt = zeros(size(vectormap));
        for i = 1:size(vectormap_gt, 4)
            vectormap_gt(:, :, :, i) = label{i}.vectormap;
        end
        PAF_loss = EuclideanLossForward(vectormap, vectormap_gt);
    end
    
%     point_vectors = [];
    point_vector_index = [];
    for i_batch = 1:net_opt.batch_size
        for i_kpt = 1:numel(label{i_batch}.keypoints)
            if isempty(label{i_batch}.keypoints(i_kpt).id)
                continue;
            end
            feat_x = floor((label{i_batch}.keypoints(i_kpt).coord(1)-1) / net_opt.stride)+1;
            feat_y = floor((label{i_batch}.keypoints(i_kpt).coord(2)-1) / net_opt.stride)+1;
            if feat_x > 46 || feat_y > 46
                disp('debug');
            end
%             point_vectors(end+1, :) = pointmap(feat_x, feat_y, :, i_batch);
            point_vector_index(end+1, 1:4) = [i_batch, feat_x, feat_y, label{i_batch}.keypoints(i_kpt).id];
        end
    end
    
%     [~, ia, ic] = unique(point_vector_index(:, 1:3), 'rows', 'stable');
%     point_vectors = point_vectors(ia, :);
%     point_vector_index_uni = zeros(numel(ia), 4);
%     for i_uni = 1:numel(ia)
%         if numel(ic(ic==i_uni)) == 1
%             point_vector_index_uni(i_uni, :) = point_vector_index(ia(i_uni), :);
%             continue;
%         end
%         to_select = point_vector_index(ic==i_uni, :);
%         to_select = to_select(randperm(size(to_select, 1), 1), :);
%         point_vector_index_uni(i_uni, :) = to_select;
%     end
    
    point_index = select_points(heatmap, pointmap, point_vector_index, label, net_opt);
    
%     if numel(find(point_index(:, 2) > 46)) > 0 || numel(find(point_index(:, 3) > 46)) > 0
%         disp('debug');
%     end
    
    pairs_feat1 = [];
    pairs_feat2 = [];
    pairs_label = [];
    pairs_batch_num = [];
    pairs_coord1 = [];
    pairs_coord2 = [];
    pairs_id = [];
    pairs_index = [];
    for i_pair = 1:net_opt.num_viewpoints:length(label)
        feat_list1 = point_index(point_index(:, 1) == i_pair, :);
        feat_list2 = point_index(point_index(:, 1) == i_pair+1, :);
        for i_joint = 1:numel(label{i_pair}.keypoints)
            joint_list1 = feat_list1(feat_list1(:, 4) == i_joint, :);
            joint_list2 = feat_list2(feat_list2(:, 4) == i_joint, :);
%             if isempty(joint_list1) || isempty(joint_list2)
%                 continue;
%             end

            if numel(find(point_index(:, 1) == i_pair & point_index(:, 4) == i_joint)) == 0 ||...
                    numel(find(point_index(:, 1) == i_pair+1 & point_index(:, 4) == i_joint)) == 0
                continue;
            end

            select1 = find(point_index(:, 1) == i_pair & point_index(:, 4) == i_joint & point_index(:, 5) == 1);
            select2 = find(point_index(:, 1) == i_pair+1 & point_index(:, 4) == i_joint & point_index(:, 5) == 1);            
            [X, Y] = ndgrid(select1, select2);
            pairs_index = [pairs_index; ones(numel(X), 1) * i_pair, reshape(X', [], 1), reshape(Y', [], 1), ones(numel(X), 1)];
            
            select1 = find(point_index(:, 1) == i_pair & point_index(:, 4) == i_joint & point_index(:, 5) == 0);
            select2 = find(point_index(:, 1) == i_pair+1 & point_index(:, 4) == i_joint & point_index(:, 5) == 1);  
            [X, Y] = ndgrid(select1, select2);
            pairs_index = [pairs_index; ones(numel(X), 1) * i_pair, reshape(X', [], 1), reshape(Y', [], 1), zeros(numel(X), 1)];
            
            select1 = find(point_index(:, 1) == i_pair & point_index(:, 4) == i_joint & point_index(:, 5) == 1);
            select2 = find(point_index(:, 1) == i_pair+1 & point_index(:, 4) == i_joint & point_index(:, 5) == 0);  
            [X, Y] = ndgrid(select1, select2);
            pairs_index = [pairs_index; ones(numel(X), 1) * i_pair, reshape(X', [], 1), reshape(Y', [], 1), zeros(numel(X), 1)];
            
            for i_points = 1:size(joint_list1, 1)
                if joint_list1(i_points, 5) == 1
                    for j_points = 1:size(joint_list2, 1)
                        %pos
                        if joint_list2(j_points, 5) == 1
                            pairs_feat1(end+1, :) = reshape(pointmap(joint_list1(i_points, 2), joint_list1(i_points, 3), :, i_pair), 1, 128);
                            pairs_feat2(end+1, :) = reshape(pointmap(joint_list2(j_points, 2), joint_list2(j_points, 3), :, i_pair+1), 1, 128);
                            pairs_label(end+1) = 1;
                            pairs_batch_num(end+1) = i_pair;
                            pairs_coord1(end+1, :) = joint_list1(i_points, 2:3);
                            pairs_coord2(end+1, :) = joint_list2(j_points, 2:3);
                            pairs_id(end+1) = i_joint;
%                             pairs{end+1} = pair;
                        elseif joint_list2(j_points, 5) == 0               
                        %neg
                            feat1 = reshape(pointmap(joint_list1(i_points, 2), joint_list1(i_points, 3), :, i_pair), 1, 128);
                            feat2 = reshape(pointmap(joint_list2(j_points, 2), joint_list2(j_points, 3), :, i_pair+1), 1, 128);
                            dist = pdist2(feat1, feat2, 'euclidean');
%                             if dist < net_opt.margin
                            pairs_feat1(end+1, :) = feat1;
                            pairs_feat2(end+1, :) = feat2;
                            pairs_label(end+1) = 0;
                            pairs_batch_num(end+1) = i_pair;
                            pairs_coord1(end+1, :) = joint_list1(i_points, 2:3);
                            pairs_coord2(end+1, :) = joint_list2(j_points, 2:3);
                            pairs_id(end+1) = i_joint;
%                                 pairs{end+1} = pair;
%                             else
%                                 disp('debug');
%                             end
                        end
                    end    
                end
            end
            for i_points = 1:size(joint_list2, 1)
                if joint_list2(i_points, 5) == 1                   
                    %neg
                    for j_points = 1:size(joint_list1, 1)
                        if joint_list1(j_points, 5) == 0
                            feat1 = reshape(pointmap(joint_list1(j_points, 2), joint_list1(j_points, 3), :, i_pair), 1, 128);
                            feat2 = reshape(pointmap(joint_list2(i_points, 2), joint_list2(i_points, 3), :, i_pair+1), 1, 128);
                            dist = pdist2(feat1, feat2, 'euclidean');
%                             if dist < net_opt.margin
                            pairs_feat1(end+1, :) = feat1;
                            pairs_feat2(end+1, :) = feat2;
                            pairs_label(end+1) = 0;
                            pairs_batch_num(end+1) = i_pair;
                            pairs_coord1(end+1, :) = joint_list1(j_points, 2:3);
                            pairs_coord2(end+1, :) = joint_list2(i_points, 2:3);
                            pairs_id(end+1) = i_joint;
%                                 pairs{end+1} = pair;
%                             else
%                                 disp('debug');
%                             end
                        end
                    end
                end
            end
        end
    end
%     
%     pos_dist = cellfun(@(x) norm(x), mat2cell((pairs_feat1(pairs_label == 1, :) - pairs_feat2(pairs_label == 1, :))', 128, ones(1, numel(find(pairs_label == 1)))));
%     neg_dist = cellfun(@(x) norm(x), mat2cell((pairs_feat1(pairs_label == 0, :) - pairs_feat2(pairs_label == 0, :))', 128, ones(1, numel(find(pairs_label == 0)))));
%     pos_min_dist = min(pos_dist);
%     pos_max_dist = max(pos_dist);
%     neg_min_dist = min(neg_dist);
%     neg_max_dist = max(neg_dist);
    
    loss = Contrastive_loss_forward(pairs_feat1', pairs_feat2', pairs_label, net_opt.margin);
    point_diff = zeros(size(caffe_solver.net.blobs('feature_output').get_diff()));
    caffe_solver.net.blobs('feature_output').set_diff(point_diff);
    [diff_feat1, diff_feat2] = Contrastive_loss_backward(...
            pairs_feat1', pairs_feat2', pairs_label, net_opt.margin);
    for i_diff = 1:size(diff_feat1, 2)
        point_diff(pairs_coord1(i_diff, 1), pairs_coord1(i_diff, 2), :, pairs_batch_num(i_diff)) = ...
            point_diff(pairs_coord1(i_diff, 1), pairs_coord1(i_diff, 2), :, pairs_batch_num(i_diff)) + reshape(diff_feat1(:, i_diff), 1, 1, 128);
        point_diff(pairs_coord2(i_diff, 1), pairs_coord2(i_diff, 2), :, pairs_batch_num(i_diff)) = ...
            point_diff(pairs_coord2(i_diff, 1), pairs_coord2(i_diff, 2), :, pairs_batch_num(i_diff)) + reshape(diff_feat2(:, i_diff), 1, 1, 128);
    end    
    caffe_solver.net.blobs('feature_output').set_diff(point_diff);
    pairs = {pairs_feat1, pairs_feat2, pairs_label, pairs_batch_num, pairs_coord1, pairs_coord2, pairs_id};
end