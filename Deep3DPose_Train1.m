function Deep3DPose_Train1()
    clc;
    clear;
    close all;
    dbstop if error;
    
%     addpath('/home/alan/caffe-ssd/matlab');
    addpath('./utils');
    
    use_gpu = true;
    gpu_id = 2;
    caffe_gpu_init(use_gpu, gpu_id);
    
    net_opt = Deep3DPose_net_options();
    net_opt.base_lr = 0.000001;  
%     net_opt.batch_size = 16;
    net_opt.solver_path = './models/pose_solver.prototxt';
    net_opt.train_proto = '/home/alan/workspace/Deep3DPose/models/pose_train_test_alt.prototxt';
    net_opt.init_file = '/home/alan/workspace/Deep3DPose/models/pose_iter_440000.caffemodel';    
    net_opt.data_path = '/media/alan/Data/Human3.6m/Subject_Data_3d_bb.mat';
    net_opt.num_person = 4;
    net_opt.frames_per_person = 1;
    net_opt.num_viewpoints = 2;
    net_opt.stride = 8;
    net_opt.heatmap.sigma = 7;
    net_opt.vectormap.theta = 1;
    net_opt.num_keypoints = 18;
    net_opt.dataset_name = 'H36M';
    net_opt.multi_task = false;
    net_opt.batch_size = net_opt.num_person * net_opt.frames_per_person * net_opt.num_viewpoints;
    net_opt.margin = 0.75;
    net_opt.step_size = 1000;
    
    dataset = data_preprocess(net_opt.data_path);
    trn_data = dataset;
    trn_data(4:5) = [];
    
%     trn_data = {trn_data{1}(:, 1:5)};
%     net_opt.num_person = 1;
%     net_opt.frames_per_person = 1; 
%     net_opt.num_viewpoints = 2;
%     net_opt.batch_size = net_opt.num_person * net_opt.frames_per_person * net_opt.num_viewpoints;
    
    tst_data = dataset(4:5);
    
    sub_folder_name = strrep(datestr(now()), ' ', '_');
    cache_path = ['./train_cache/', [net_opt.dataset_name, '_gpu', num2str(gpu_id)], '/', sub_folder_name];
    MakeDirIfMissing(cache_path);
    MakeDirIfMissing([cache_path, '/log/']);
    
    ChangeSolverFile(net_opt.solver_path, net_opt.train_proto);
    caffe_solver = caffe.Solver(net_opt.solver_path);
    caffe_solver.net.blobs('data').reshape([net_opt.image_size, net_opt.image_size, 3, net_opt.num_person * net_opt.frames_per_person * net_opt.num_viewpoints]);
    caffe_solver.net.copy_from(net_opt.init_file);    
    
    all_time = 0;
    show_interval = 100;
    train_ap = 0;
    
    loss_record = zeros(1, net_opt.max_iter);
    ap_record = zeros(1, net_opt.max_iter);
    train_ap_record = zeros(1, net_opt.max_iter);    
    
    
    hf = figure(1);
    set(hf,'visible','off')
%     [data_batch, label_batch] = generate_train_batch(trn_data, net_opt);   
%     
%     point_vector_index = [];
%     for i_batch = 1:net_opt.batch_size
%         for i_kpt = 1:numel(label_batch{i_batch}.keypoints)
%             if isempty(label_batch{i_batch}.keypoints(i_kpt).id)
%                 continue;
%             end
%             feat_x = floor((label_batch{i_batch}.keypoints(i_kpt).coord(1)-1) / net_opt.stride)+1;
%             feat_y = floor((label_batch{i_batch}.keypoints(i_kpt).coord(2)-1) / net_opt.stride)+1;
% %             if feat_x > 46 || feat_y > 46
% %                 disp('debug');
% %             end
% %             point_vectors(end+1, :) = pointmap(feat_x, feat_y, :, i_batch);
%             point_vector_index(end+1, 1:4) = [i_batch, feat_x, feat_y, label_batch{i_batch}.keypoints(i_kpt).id];
%         end
%     end
% 
%     point_index = select_points(point_vector_index, label_batch, net_opt);
    pos_dist_record = [];
    neg_dist_record = [];
    for i_trn = 1:net_opt.max_iter
        
        [data_batch, label_batch] = generate_train_batch(trn_data, net_opt);
        
        data_prepare_time = tic;
        data_batch = CaffeInputPreProcess(data_batch, label_batch, 'VGG', net_opt);
        data_prepare_time = toc(data_prepare_time);
        
        forward_time = tic;
        caffe_solver.net.forward({data_batch});
        forward_time = toc(forward_time);
        
%         if net_opt.multi_task
        sample_time = tic;
        loss_stage6_L1 = caffe_solver.net.blobs('Mconv7_stage6_L1').get_data();        
        loss_stage6_L2 = caffe_solver.net.blobs('Mconv7_stage6_L2').get_data();
%         else
%             loss_stage6_L1 = [];
%             loss_stage6_L2 = [];
%         end
        feature_pm = caffe_solver.net.blobs('feature_norm').get_data();
        sample_time = toc(sample_time);
        
        loss_time = tic;        
        [loss, ~] = D3PLossForwardBackward(caffe_solver, loss_stage6_L2, loss_stage6_L1, feature_pm, label_batch, net_opt);
        loss_time = toc(loss_time);
        
        back_time = tic;
        caffe_solver.net.backward_prefilled();
        rate_now = net_opt.base_lr * ...
                net_opt.lr_gamma^(floor(i_trn / net_opt.step_size));
        caffe_solver.update(single(rate_now));
        back_time = toc(back_time);
        
        [point_acc, pdrec, ndrec] = PointsEvaluate(feature_pm, label_batch, 0, net_opt);
        pos_dist_record = [pos_dist_record; pdrec];
        neg_dist_record = [neg_dist_record; ndrec];
        train_ap = train_ap + point_acc;
        
        loss_record(i_trn) = loss;
        
        all_time = all_time + data_prepare_time + forward_time + sample_time + loss_time + back_time;
        fprintf( '%s  i_trn %6d / %6d  loss:%.5f  accuracy:%.5f\tlr:%.6f\n', ...
            datestr(now()), i_trn, net_opt.max_iter, loss, point_acc, rate_now);
        fprintf( 'data:%2.2f, forward:%2.4f, sample:%2.2f, loss:%2.2f, back:%2.2f, all:%2.2f, estimate time:%2.2fh\n\n', ...
            data_prepare_time, forward_time, sample_time, loss_time, back_time, ...
            data_prepare_time + forward_time + sample_time + loss_time + back_time, ...
            all_time / i_trn * (net_opt.max_iter - i_trn) / 3600);
        if ~mod(i_trn, show_interval)
            X = 1 : i_trn;
            set(0,'CurrentFigure',hf);subplot(3, 1, 1);
            line(X, loss_record(1 : i_trn));
            title(sprintf('Training Loss, iter-%d.jpg', i_trn));
            
            train_ap = train_ap / show_interval;
            v = (1 : show_interval) ./ show_interval;
            t = i_trn - show_interval;
            if t >= 1
                u = train_ap_record(t) + (mean(train_ap) - train_ap_record(t)) .* v;
            else
                u = ones(1, show_interval) * mean(train_ap);
            end
            train_ap_record(t + 1 : i_trn) = u;
            train_ap = 0;
            
            set(0,'CurrentFigure',hf);subplot(3, 1, 2);
            line(X, train_ap_record(1 : i_trn));
            title(sprintf('Train AP = %2.2f, iter-%d.jpg', train_ap_record(i_trn), i_trn));
            
            test_ap = Deep3DPose_Test(tst_data, caffe_solver.net, net_opt);
            v = (1 : show_interval) ./ show_interval;
            t = i_trn - show_interval;
            if t >= 1
                u = ap_record(t) + (mean(test_ap) - ap_record(t)) .* v;
            else
                u = ones(1, show_interval) * mean(test_ap);
            end
            ap_record(t + 1 : i_trn) = u;
            
            set(0,'CurrentFigure',hf);subplot(3, 1, 3);
            line(X, ap_record(1 : i_trn), 'Color', 'b');
            title(sprintf('Test AP = %2.2f, iter-%d.jpg', ap_record(i_trn), i_trn));
            
            save('./dist_record.mat', 'pos_dist_record', 'neg_dist_record');
            
            saveas(1, [sprintf('./training_curve_%d.jpg', gpu_id)]);
            pause(5);
        end
        if ~mod(i_trn, net_opt.save_iter)
           caffe_solver.net.save([cache_path, ...
                '/cached_net_iter_', num2str(i_trn), '.caffemodel']);
        end
    end
    
end