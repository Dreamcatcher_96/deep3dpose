function heatmap = generate_heatmap(kpt, net_opt)
%%%TODO:background heatmap gen
    image_size = net_opt.image_size;
    stride = net_opt.stride;
    sigma = net_opt.heatmap.sigma;
    
    heatmap = zeros(image_size / stride, image_size / stride, net_opt.num_keypoints+1);
    [height, width, num_points] = size(heatmap);
    
    start = stride / 2 - 0.5;
    
    for i = 1:numel(kpt)
        if isempty(kpt(i).id)
            continue;
        end
        x = kpt(i).coord(1);
        y = kpt(i).coord(2);
        
        for h = 1:height
            for w = 1:width
                xx = start + w * stride;
                yy = start + h * stride;
                dis = ((xx - x) * (xx - x) + (yy - y) * (yy - y)) / 2 / sigma / sigma;
                if dis > 4.6052
                    continue;
                end
                heatmap(h, w, i) = heatmap(h, w, i) + exp(-dis);
                if heatmap(h, w, i) > 1
                    heatmap(h, w, i) = 1;
                end
            end
        end
    end
    
end