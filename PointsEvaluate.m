function [accuracy, pos_dist_record, neg_dist_record] = PointsEvaluate(pointsmap, label, vis, net_opt)
    accuracy = 0;
    if vis
        figure(2);
    end
    pos_dist_record = [];
    neg_dist_record = [];
    for i = 1:net_opt.num_viewpoints:length(label)
        predict = 0;
%         feats_1 = zeros(numel(label{i}.keypoints), 128);
        keypoints = extractfield(label{i}.keypoints, 'coord');
        keypoints = cell2mat(keypoints(~cellfun(@isempty, keypoints))');
        feats_1 = zeros(size(keypoints, 1), 128);
        for ii = 1:size(keypoints, 1)
            pos_index = floor([keypoints(ii, 1) / net_opt.image_size * size(pointsmap, 1),...
                keypoints(ii, 2) / net_opt.image_size * size(pointsmap, 2)]);
%             floor((label{i_batch}.keypoints(i_kpt).coord(1)-1) / net_opt.stride)+1;
            feats_1(ii, :) = pointsmap(pos_index(1), pos_index(2), :, i);
        end
        feats_2 = pointsmap(:, :, :, i+1);
        dist_metric = pdist2(reshape(feats_1, [], 128), reshape(feats_2, [], 128), 'euclidean');
%         [min_dist, dist_index] = min(dist_metric, [], 2);
        
        if vis
            dimg = imread(label{i+1}.img_path);
%             [height, width, ~] = size(dimg);
            dimg = imresize(dimg, [368, 368]);
            imshow(dimg);
            hold on;
        end
        height = label{i}.img_size(1);
        width = label{i}.img_size(2);
        
        for i_index = 1:size(dist_metric, 1)
            [dist_value, dist_index] = min(dist_metric(i_index, :));    %dist_metric(i_index, :) <= net_opt.margin
            if dist_value > net_opt.margin
                dist_index = [];
            end
            if numel(dist_index) == 0 && isempty(label{i+1}.keypoints(i_index).id)
                predict = predict + 1;
                continue;
            elseif numel(dist_index) == 1 && isempty(label{i+1}.keypoints(i_index).id)
                continue;
            elseif numel(dist_index) == 0 && ~isempty(label{i+1}.keypoints(i_index).id)
                continue;
            end
            if numel(dist_index) == 0 || isempty(label{i+1}.keypoints(i_index).id)
                continue;
            end
            [pred_coord(1), pred_coord(2)] = ind2sub([size(pointsmap, 1), size(pointsmap, 2)], dist_index);
            gt_coord = floor([label{i+1}.keypoints(i_index).coord(1) / net_opt.image_size * size(pointsmap, 1),...
                label{i+1}.keypoints(i_index).coord(2) / net_opt.image_size * size(pointsmap, 2)]);
            
            crop_size = single([label{i+1}.crop_coord(3)-label{i+1}.crop_coord(1), label{i+1}.crop_coord(4)-label{i+1}.crop_coord(2)]);
            
            real_pred(1) = (pred_coord(1) / size(pointsmap, 1) * crop_size(1) + single(label{i+1}.crop_coord(1))) / width * 368;
            real_pred(2) = (pred_coord(2) / size(pointsmap, 2) * crop_size(2) + single(label{i+1}.crop_coord(2))) / height * 368;

            real_gt(1) = (label{i+1}.keypoints(i_index).coord(1) / net_opt.image_size * crop_size(1) + single(label{i+1}.crop_coord(1))) / width * net_opt.image_size;
            real_gt(2) = (label{i+1}.keypoints(i_index).coord(2) / net_opt.image_size * crop_size(2) + single(label{i+1}.crop_coord(2))) / height * net_opt.image_size;
            
            k3d = label{i+1}.keypoints(i_index).coord_3d;
            l2_dist = pdist2(pred_coord, gt_coord, 'euclidean');
%             if l2_dist < 1 / k3d(3) * net_opt.in_ratio / label{i+1}.zoom_scale
            if l2_dist < 1.5
                pos_dist_record = [pos_dist_record; dist_value];
                predict = predict + 1;
                if vis
                    plot(real_gt(1), real_gt(2), '.', 'Color', 'b');
                    plot(real_pred(1), real_pred(2), '.', 'Color', 'g');
                end
            else
                neg_dist_record = [neg_dist_record; dist_value];
                if vis
                    plot(real_gt(1), real_gt(2), '.', 'Color', 'b');
                    plot(real_pred(1), real_pred(2), '.', 'Color', 'r');
                end
            end
        end
        accuracy = accuracy + predict / size(dist_metric, 1);
        if vis
            hold off;
            pause(0.5);
        end
    end
    accuracy = accuracy / numel(1:net_opt.num_viewpoints:length(label));
end