function net_opt = Deep3DPose_net_options()
    net_opt.image_size = 368;
    net_opt.max_iter = 500000;
    net_opt.save_iter = 1000;
    net_opt.lr_gamma = 0.1;
    net_opt.step_size = 100;
    net_opt.vector_pair = [2, 3, 5, 6, 8, 9, 11, 12, 0, 1, 1, 1, 1, 2, 5, 0, 0, 14, 15;...
                           3, 4, 6, 7, 9, 10, 12, 13, 1, 8, 11, 2, 5, 16, 17, 14, 15, 16, 17] + 1;
                       
    net_opt.in_ratio = 4000;    
    net_opt.out_ratio = 8000;
end