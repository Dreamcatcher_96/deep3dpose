function vectormap = generate_vectormap(kpt, net_opt)
    image_size = net_opt.image_size;
    stride = net_opt.stride;
    theta = net_opt.vectormap.theta;
    vec_pair = net_opt.vector_pair;
    
    vectormap = zeros(image_size / stride, image_size / stride, size(net_opt.vector_pair, 2) * 2);
    countmap = zeros(image_size / stride, image_size / stride, size(net_opt.vector_pair, 2));
    
    [height, width, channel] = size(countmap);
    
    for i = 1:channel
        a = vec_pair(1, i);
        b = vec_pair(2, i);
        if isempty(kpt(a).id) || isempty(kpt(b).id)
            continue;
        end
        ax = kpt(a).coord(1) / stride;
        ay = kpt(a).coord(2) / stride;
        bx = kpt(b).coord(1) / stride;
        by = kpt(b).coord(2) / stride;
        
        bax = bx - ax;
        bay = by - ay;
        norm_ba = sqrt(bax * bax + bay * bay) + 1*10^-9;
        bax = bax / norm_ba;
        bay = bay / norm_ba;
        
        min_w = max([round(min([ax, bx]) - theta), 0]);
        max_w = min([round(max([ax, bx]) + theta), width]);
        min_h = max([round(min([ay, by]) - theta), 0]);
        max_h = min([round(max([ay, by]) + theta), height]);
        
        for h = min_h:max_h
            for w = min_w:max_w
                px = w - ax;
                py = h - ay;
                
                dis = abs(bay * px - bax * py);
                if dis <= theta
                    vectormap(h, w, 2*(i-1)+1) = ...
                                (vectormap(h, w, 2*(i-1)+1) * countmap(h, w, i) + bax) / (countmap(h, w, i) + 1);
                    vectormap(h, w, 2*(i-1)+2) = ...
                                (vectormap(h, w, 2*(i-1)+2) * countmap(h, w, i) + bay) / (countmap(h, w, i) + 1);
                    countmap(h, w, i) = countmap(h, w, i) + 1;
                end
            end
        end
    end
end