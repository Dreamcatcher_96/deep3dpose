function neg_test(Heatmap, label, in_ratio, out_ratio, thresh, upper, isrand)
    figure
    for i_joint = 1:18
        for i_num = 1:numel(label)
            if isempty(label{i_num}.keypoints(i_joint).id)
                continue;
            end
            dimg = imread(label{i_num}.img_path);
            bbox_pred_now = label{i_num}.crop_coord;
            
            dimg = dimg(bbox_pred_now(2):bbox_pred_now(4), bbox_pred_now(1):bbox_pred_now(3), :);
            dimg = imresize(dimg, [46, 46]);
            heatmap = permute(Heatmap(:, :, :, i_num), [2, 1, 3]);
            debug = heatmap(:, :, i_joint);
            debug = (debug - min(debug(:))) ./ (max(debug(:)) - min(debug(:)));
        %     debug = imresize(debug, [368, 368]);
            if isrand
                debug = debug - rand(size(debug));
            end
    %         debug = imfuse(dimg, debug);
            keypoints = label{i_num}.keypoints;

            [y, x] = ind2sub([size(debug, 1), size(debug, 2)], find(and(debug <= upper, debug >= thresh)));
            imshow(dimg);
            hold on;   
            k3d = keypoints(i_joint).coord_3d;
            k2d = keypoints(i_joint).coord;
    %         k2d(1) = k2d(1) / 368 * size(debug, 2);
    %         k2d(2) = k2d(2) / 368 * size(debug, 1);
%             real_gt(1) = (label{i_num}.keypoints(i_index).coord(1) / net_opt.image_size * crop_size(1) + single(label{i_num}.crop_coord(1))) / width * net_opt.image_size;
%             real_gt(2) = (label{i_num}.keypoints(i_index).coord(2) / net_opt.image_size * crop_size(2) + single(label{i_num}.crop_coord(2))) / height * net_opt.image_size;
            k2d(1) = floor((keypoints(i_joint).coord(1)-1) / 8)+1;
            k2d(2) = floor((keypoints(i_joint).coord(2)-1) / 8)+1;
            plot(k2d(1), k2d(2), '*');
            for ii = 1:numel(x)
                dist = norm([x(ii), y(ii)] - k2d(1:2));
                if dist < in_ratio   %1 / k3d(3) * in_ratio / label{i_num}.zoom_scale
                    plot(x(ii), y(ii), '.', 'Color', 'r');
                elseif dist > out_ratio    %1 / k3d(3) * out_ratio / label{i_num}.zoom_scale
                    plot(x(ii), y(ii), '.', 'Color', 'b');
                end
            end
            hold off;
            disp(k3d(3));
            pause(0.5);
        end
    end
end